//Desenvolva um sistema simples para controle de estoque, contendo pelo menos as classes Produto e Estoque, e as seguintes opera��es: 
//1. Manter as propriedades dos produtos (nome, pre�o, quantidade em estoque).
//2. Verificar que produtos precisam ser repostos.
//3. Retirar um produto do estoque e,
//4. Listar o saldo em reais do estoque.

import java.util.Scanner;

public class APP {

	static int index = 0;

	static Produto[] lista = new Produto[5];

	static Scanner tecla = new Scanner(System.in);

	public static void main(String[] args) {
		int op;
		do {
			System.out.println("*** MENU PRINCIPAL ***");
			System.out.println("1-Incluir produto");
			System.out.println("2-Verificar produtos em falta");
			System.out.println("3-Excluir produto");
			System.out.println("4-Listar saldo em reais do estoque");
			System.out.println("5-Sair");
			System.out.println("Digite sua op��o: ");
			op = tecla.nextInt();
			switch (op) {
			case 1:
				incluirProduto();
				break;
			case 2:
				verificarProduto();
				break;
			case 3:
				excluirProduto();
				break;
			case 4:
				listarSaldo();
				break;
			case 5:
				break;
			}
		} while (op != 5);
	}

	public static void incluirProduto() {
		// Entrada
		System.out.println("Digite o nome do produto:");
		String nome = tecla.next();
		System.out.println("Digite o pre�o do produto:");
		double preco = tecla.nextDouble();
		System.out.println("Digite o quantidade do produto:");
		int quantidade = tecla.nextInt();
		// Criar o objeto e inserir na lista
		lista[index++] = new Produto(nome, preco, quantidade);
		System.out.println("Produto cadastrado!");
	}

	public static void verificarProduto() {
		for (int i = 0; i < lista.length - 1; i++) {
			if (0 == lista[i].getQuantidade()) {
				System.out.println("Produto " + lista[i].getNome() + " em falta.");
				break;
			}
		}
	}

	public static void excluirProduto() {
		System.out.println("Digite o nome do produto:");
		String nome = tecla.next();

		for (int i = 0; i < lista.length - 1; i++) {
			if (nome == lista[i].getNome()) {
				lista[i] = null;
				System.gc();
				break;
			}
		}
	}

	public static void listarSaldo() {
		double total = 0;
		System.out.println("SALDO TOTAL:");
		for (int i = 0; i < lista.length - 1; i++) {
			if (lista[i] != null) {
				// total += lista[i].getSaldo();
				total = total + (lista[i].getPreco() * lista[i].getQuantidade());
			} else {
				break;
			}
		}
		System.out.println("Total em reais:........" + total);
	}

}
