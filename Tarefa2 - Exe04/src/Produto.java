public class Produto {

    private final static double TAXA = 8.9;
    
    static int contador = 0;
    
    //Atributos de instancia
    private int quantidade;
    private double preco;
    private String nome;

	public int getQuantidade() {
		return quantidade;
	}
    
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
    
   
    public Produto(String nome, double preco, int quantidade){
        this.nome = nome;
        this.preco = preco;
        this.quantidade = quantidade;
    }
}


