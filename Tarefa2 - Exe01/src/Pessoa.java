public class Pessoa{ 
	  
    private int cod;  
    private String nome;  
    
    public int getCod() {
        return cod;
    }

    public void setCod(int c) {
        this.cod = cod;
    }
    
    
    public String getNome() {
        return nome;
    }

    public void setNome(int n) {
        this.nome = nome;
    }
     
    public Pessoa(int cod, String nome){  
    	this.cod = cod; 
    	this.nome = nome;    
    } 
    
} 