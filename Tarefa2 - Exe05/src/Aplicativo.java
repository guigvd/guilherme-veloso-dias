import java.util.*;

public class Aplicativo {

	static List<Aluno> lista = new ArrayList<Aluno>();

	static Scanner tecla = new Scanner(System.in);

	public static void main(String[] args) {
		int op;
		do {
			System.out.println("---MENU---");
			System.out.println("1-Inserir aluno");
			System.out.println("2-Excluir aluno");
			System.out.println("3-Lan�ar/Editar Notas");
			System.out.println("4-Consultar todos os alunos");
			System.out.println("5-Consultar situa��o do aluno");
			System.out.println("6-Sair");
			System.out.println("Digite sua op��o: ");
			op = tecla.nextInt();
			switch (op) {
			case 1:
				inserirAluno();
				break;
			case 2:
				excluirAluno();
				break;
			case 3:
				alterarNota();
				break;
			case 4:
				listarAlunos();
				break;
			case 5:
				consultarAluno();
				break;
			case 6:
				break;
			}
		} while (op != 6);
	}

	public static void inserirAluno() {
		System.out.println("Digite a matr�cula:");
		int mat = tecla.nextInt();
		System.out.println("Digiteo nome:");
		String nome = tecla.next();
		lista.add(new Aluno(mat, nome));
	}

	public static void excluirAluno() {
		System.out.println("Digite a matr�cula:");
		int mat = tecla.nextInt();
		for (Aluno l : lista) {
			if (l.getMatricula() == mat) {
				lista.remove(l);
				break;
			}
		}
	}

	public static void alterarNota() {
		System.out.println("Digite a matr�cula:");
		int mat = tecla.nextInt();
		for (Aluno lst : lista) {
			if (lst.getMatricula() == mat) {
				System.out.println("Digite  AV 1:");
				lst.setAv1(tecla.nextDouble());
				System.out.println("Digite  AV 2:");
				lst.setAv2(tecla.nextDouble());
				break;
			}
		}
	}

	public static void listarAlunos() {
		if (lista.isEmpty() == false) {
			System.out.println("Lista de alunos cadastrados : ");
			for (Aluno lst : lista) {
				lst.print();
			}
		} else {
			System.out.println("Lista vazia!  Nenhum aluno cadastrado.");
		}
	}

	public static void consultarAluno() {
		System.out.println("Digite a matr�cula:");
		int m = tecla.nextInt();
		for (Aluno lst : lista) {
			if (lst.getMatricula() == m) {
				double media = (lst.getAv1() + lst.getAv2()) / 2;
				if (media >= 6.0) {
					System.out.println("AV1:  " + lst.getAv1());
					System.out.println("AV2:  " + lst.getAv2());
					System.out.println("M�dia:  " + media);
					System.out.println("Aprovado !");
				} else if (media < 6.0) {
					System.out.println("AV1:" + lst.getAv1());
					System.out.println("AV2:" + lst.getAv2());
					System.out.println("M�dia:" + media);
					System.out.println("Reprovado!");
				}
			}
		}
	}
}
