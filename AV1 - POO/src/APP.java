import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class APP {

	static ArrayList<Pessoa> lista = new ArrayList<>();

	static Scanner tecla = new Scanner(System.in);

	static int index = 0;

	public static void main(String[] args) {
		int op;
		do {
			System.out.println("\n---------MENU---------");
			System.out.println("1- Incluir Pessoa");
			System.out.println("2- Listar Pessoas (ordem crescente de idade)");
			System.out.println("3- Consultar Pessoa");
			System.out.println("4- Listar Cache (para validar)");
			System.out.println("5- Sair");
			
			op = tecla.nextInt();
			switch (op) {
			case 1:
				incluirPessoa();
				break;
			case 2:
				listarPessoa();
				break;
			case 3:
				consultarPessoa();
				break;
			case 4:
				CachePessoa.listarCache();
				break;

			case 5:
				break;
			}
		} while (op != 4);
	}

	private static void incluirPessoa() {
		System.out.println("Digite o ID:");
		int id = tecla.nextInt();
		System.out.println("Digite o nome:");
		String nome = tecla.next();
		System.out.println("Digite a idade:");
		int idade = tecla.nextInt();
		lista.add(new Pessoa(id, nome, idade));
	}

	public static void consultarPessoa() {
		// Entrada
		System.out.println("Digite o ID da pessoa:");
		int id = tecla.nextInt();
		CachePessoa.retornaPessoa(id);
	}

	public static void varrerListaPrincipal(int id) {
		// Procurar a pessoa na lista principal
		for (Pessoa Pessoa : lista) {
			if (Pessoa.getId() == id) {
				imprimir(Pessoa);
				String nome = Pessoa.getNome();
				int idade = Pessoa.getIdade();
				CachePessoa.adicionaCache(id, nome, idade);
				break;
			}
		}
	}

	public static void ordenarLista() {
		Collections.sort(lista);
	}

	public static void imprimir(Pessoa p) {
		p.print();
	}

	public static void listarPessoa() {
		System.out.println("=======================");
		System.out.println("ID:  IDADE:  NOME:");
		System.out.println("=======================");

		ordenarLista();

		for (Pessoa pessoa : lista) {

			if (!pessoa.equals(null)) {

				imprimir(pessoa);

			}
		}
	}
}