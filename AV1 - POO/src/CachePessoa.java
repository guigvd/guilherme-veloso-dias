import java.util.ArrayList;
import java.util.Scanner;

public class CachePessoa extends Pessoa {

	static Scanner tecla = new Scanner(System.in);

	public CachePessoa(int id, String nome, int idade) {
		super(id, nome, idade);
	}

	static ArrayList<CachePessoa> lista = new ArrayList<>();

	public static void retornaPessoa(int id) {
		if (lista.contains(id)) {
			// Procurar a pessoa na lista
			for (CachePessoa CachePessoa : lista) {
				if (CachePessoa.getId() == id) {
					APP.imprimir(CachePessoa);
				}
			}
		} else
			APP.varrerListaPrincipal(id);

	}
	
	public static void adicionaCache(int id, String nome, int idade) {
		lista.add(new CachePessoa(id, nome, idade));
	}

	
	//Lista cache para validar o funcionamento l�gico da atividade
	public static void listarCache() {

		for (CachePessoa CachePessoa : lista) {

			if (!CachePessoa.equals(null)) {

				APP.imprimir(CachePessoa);

			}
		}
	}

	
}
