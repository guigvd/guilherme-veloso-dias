public class Pessoa implements Comparable<Pessoa>{ 
	  
    private int id, idade;  
    private String nome;  
    
    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
     
    public Pessoa(int id, String nome, int idade){  
    	this.id = id; 
    	this.nome = nome;    
    	this.idade = idade;
    } 
    
    // M�todo para imprimir
    public void print() {
		System.out.println(getId() + "\t" +
				           getIdade() + "\t" +
				           getNome() + "\t");
	}

	//@Override
	public int compareTo(Pessoa o) {
		if (this.idade < o.getIdade()) { 
			return -1; 
		} 
		if (this.idade > o.getIdade()) { 
			return 1;
		} 
		return 0;
	}
} 
